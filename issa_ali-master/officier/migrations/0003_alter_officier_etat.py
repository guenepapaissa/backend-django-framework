# Generated by Django 4.0.3 on 2022-04-05 19:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('officier', '0002_officier_etat'),
    ]

    operations = [
        migrations.AlterField(
            model_name='officier',
            name='etat',
            field=models.BooleanField(default=True),
        ),
    ]
