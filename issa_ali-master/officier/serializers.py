from rest_framework.serializers import ModelSerializer
from .models import Officier

        
class OfficierSerializer(ModelSerializer):
    class Meta:
        model = Officier
        fields = '__all__'
        
