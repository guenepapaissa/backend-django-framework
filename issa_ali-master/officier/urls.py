from django import views
from django.urls import path
from officier.views import officier_list,officier_detail
urlpatterns = [
    path('officier/', officier_list ),
    path('officier/<int:pk>', officier_detail),
]
