from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import *
from .serializers import *


@api_view(['GET', 'POST'])
def officier_list(request):
    
    
    if request.method == 'GET':
        officiers = Officier.objects.all()
        serializer = OfficierSerializer(officiers, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = OfficierSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'PUT', 'DELETE'])
def officier_detail(request, pk):
    try:
        officier = Officier.objects.get(pk=pk)
    except Officier.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = OfficierSerializer(officier)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = OfficierSerializer(officier, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        officier.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
