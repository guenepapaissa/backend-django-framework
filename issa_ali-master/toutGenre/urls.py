from django import views
from django.urls import path
from toutGenre.views import eneleverMotifToutGenre, mes_toutGenre, toutGenre_list,toutGenre_detail
urlpatterns = [
    path('toutGenre/', toutGenre_list ),
    path('toutGenre/<int:pk>', toutGenre_detail),
    path('mestoutGenre/<int:pk>', mes_toutGenre),
     path('eneleverMotifGenre/<int:pk>', eneleverMotifToutGenre),
]
