from client.models import Client
from django.db import models
from demande.models import Motifs
from agent.models import Agent

Certificat = (
    ('Residence','Residence'),
     ('Vie_collective','Vie_collective'),
    ('Vie_individuelle','Vie_individuelle'),
)
 

class ToutGenre(models.Model):
    date = models.DateField(auto_now=True)
    etat = models.BooleanField(default=False)
    client = models.ForeignKey(Client,null=True, on_delete=models.CASCADE)
    motif =models.ForeignKey(Motifs,null=True, on_delete=models.CASCADE)
    agent = models.ForeignKey(Agent,null=True, on_delete=models.CASCADE)
    
    adresse = models.CharField(max_length=255)
    numeroRegistre = models.IntegerField()
    annee =models.IntegerField()
    nombreExemplaire = models.IntegerField()
    etatRetrait = models.BooleanField(default=False)
    cerificat = models.CharField(max_length=255, choices=Certificat)
