from http import client
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import *
from .serializers import *


@api_view(['GET', 'POST'])
def toutGenre_list(request):
    
    
    if request.method == 'GET':
        toutGenres = ToutGenre.objects.all()
        serializer = ToutGenreSerializer(toutGenres, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = ToutGenreSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'PUT', 'DELETE'])
def toutGenre_detail(request, pk):
    try:
        toutGenre = ToutGenre.objects.get(pk=pk)
    except ToutGenre.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = ToutGenreSerializer(toutGenre)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = ToutGenreSerializer(toutGenre, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        ToutGenre.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

@api_view(['GET'])
def eneleverMotifToutGenre(request,pk):
    try:
        toutGenre = ToutGenre.objects.get(pk=pk)
        toutGenre.motif=None
        toutGenre.etat=False
        toutGenre.save()
        serializer = ToutGenreSerializer(toutGenre,many=False)
        return Response(serializer.data)
    except ToutGenre.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['GET', 'PUT', 'DELETE'])
def mes_toutGenre(request, pk):
    try:
        toutGenre = ToutGenre.objects.filter(client=pk)
    except ToutGenre.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = ToutGenreSerializer(toutGenre,many=True)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = ToutGenreSerializer(toutGenre, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        ToutGenre.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
