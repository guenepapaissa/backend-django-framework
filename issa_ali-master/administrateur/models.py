from django.db import models

# Create your models here.
class Administrateur(models.Model):
    prenom = models.CharField(max_length=255)
    nom = models.CharField(max_length=255)
    telephone = models.CharField(max_length=255)
    email = models.EmailField()
    login = models.CharField(max_length=255, unique=True)
    password = models.CharField(max_length=255)
    etat = models.BooleanField(default=True)
    