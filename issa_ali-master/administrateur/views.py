from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import *
from .serializers import *


@api_view(['GET', 'POST'])
def administrateur_list(request):
    
    
    if request.method == 'GET':
        administrateurs = Administrateur.objects.all()
        serializer = AdministrateurSerializer(administrateurs, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = AdministrateurSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'PUT', 'DELETE'])
def administrateur_detail(request, pk):
    try:
        administrateur = Administrateur.objects.get(pk=pk)
    except Administrateur.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = AdministrateurSerializer(administrateur)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = AdministrateurSerializer(administrateur, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        Administrateur.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
