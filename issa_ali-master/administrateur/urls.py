from django import views
from django.urls import path
from .views import administrateur_list,administrateur_detail
urlpatterns = [
    path('administrateur/', administrateur_list ),
    path('administrateur/<int:pk>', administrateur_detail),
]
