from rest_framework.serializers import ModelSerializer
from .models import *

class AdministrateurSerializer(ModelSerializer):
    class Meta:
        model = Administrateur
        fields = '__all__'