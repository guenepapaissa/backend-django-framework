# Generated by Django 4.0.3 on 2022-04-05 19:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('administrateur', '0002_administrateur_etat'),
    ]

    operations = [
        migrations.AlterField(
            model_name='administrateur',
            name='etat',
            field=models.BooleanField(default=True),
        ),
    ]
