from http import client
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import *
from .serializers import *


@api_view(['GET', 'POST'])
def renouvellement_list(request):
    
    
    if request.method == 'GET':
        renouvellements = Renouvellement.objects.all()
        serializer = RenouvellementSerializer(renouvellements, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = RenouvellementSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'PUT', 'DELETE'])
def renouvellement_detail(request, pk):
    try:
        renouvellement = Renouvellement.objects.get(pk=pk)
    except Renouvellement.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = RenouvellementSerializer(renouvellement)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = RenouvellementSerializer(renouvellement, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        Renouvellement.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

@api_view(['GET'])
def eneleverMotifRenouvellement(request,pk):
    try:
        renouvellement = Renouvellement.objects.get(pk=pk)
        renouvellement.motif=None
        renouvellement.etat=False
        renouvellement.save()
        serializer = RenouvellementSerializer(renouvellement,many=False)
        return Response(serializer.data)
    except Renouvellement.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

@api_view(['GET', 'PUT', 'DELETE'])
def mes_renouvellement(request, pk):
    try:
        renouvellement = Renouvellement.objects.filter(client=pk)
    except Renouvellement.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = RenouvellementSerializer(renouvellement,many=True)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = RenouvellementSerializer(renouvellement, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        Renouvellement.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
