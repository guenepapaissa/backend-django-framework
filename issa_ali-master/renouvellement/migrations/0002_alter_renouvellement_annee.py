# Generated by Django 4.0.3 on 2022-04-02 02:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('renouvellement', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='renouvellement',
            name='annee',
            field=models.IntegerField(),
        ),
    ]
