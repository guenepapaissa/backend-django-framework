from django.apps import AppConfig


class RenouvellementConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'renouvellement'
