from rest_framework.serializers import ModelSerializer
from .models import *
        
class RenouvellementSerializer(ModelSerializer):
    class Meta:
        model = Renouvellement
        fields = '__all__'
        
