from django import views
from django.urls import path
from renouvellement.views import eneleverMotifRenouvellement, mes_renouvellement, renouvellement_list,renouvellement_detail
urlpatterns = [
    path('renouvellement/', renouvellement_list ),
    path('renouvellement/<int:pk>', renouvellement_detail),
    path('mesrenoullement/<int:pk>', mes_renouvellement),
    path('eneleverMotifRenouvellement/<int:pk>', eneleverMotifRenouvellement),
]
