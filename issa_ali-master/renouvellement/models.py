import datetime
from django import forms
from client.models import Client
from django.db import models
from demande.models import Motifs
from agent.models import Agent

Extrait = (
    ('Naissance','Naissance'),
     ('Mariage','Mariage'),
    ('Deces','Deces'),
)

YEAR_CHOICES = [(y,y) for y in range(1800, datetime.date.today().year+1)]

class Renouvellement(models.Model):
    date = models.DateField(auto_now=True)
    etat = models.BooleanField(default=False)
    client = models.ForeignKey(Client,null=True, on_delete=models.CASCADE)
    motif =models.ForeignKey(Motifs,null=True, on_delete=models.CASCADE)
    agent = models.ForeignKey(Agent,null=True, on_delete=models.CASCADE)   
    numeroRegistre = models.IntegerField()
    annee =models.IntegerField()
    nombreExemplaire = models.IntegerField()
    etatRetrait = models.BooleanField(default=False)
    extrait = models.CharField(max_length=255, choices=Extrait)
