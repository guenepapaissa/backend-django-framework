from django import views
from django.urls import path


from .views import agent_list,agent_detail
urlpatterns = [
    path('agent/', agent_list ),
    path('agent/<int:pk>', agent_detail),
    
    # path('guichet/', guichet_list ),
    # path('guichet/<int:pk>', guichet_detail),
]
