from rest_framework.serializers import ModelSerializer
from .models import *

class AgentSerializer(ModelSerializer):
    class Meta:
        model = Agent
        fields = '__all__'
        
# class GuichetsSerializer(ModelSerializer):
#     class Meta:
#         model = Guichets
#         fields = '__all__'
