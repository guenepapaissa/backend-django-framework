from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import *
from .serializers import *


@api_view(['GET', 'POST'])
def agent_list(request):
    
    
    if request.method == 'GET':
        administrateurs = Agent.objects.all()
        serializer = AgentSerializer(administrateurs, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = AgentSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'PUT', 'DELETE'])
def agent_detail(request, pk):
    try:
        administrateur = Agent.objects.get(pk=pk)
    except Agent.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = AgentSerializer(administrateur)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = AgentSerializer(administrateur, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        Agent.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    
    
# //-------------------------------------------Guichet-----------------------------

# @api_view(['GET', 'POST'])
# def guichet_list(request):
    
    
#     if request.method == 'GET':
#         guichets = Agent.objects.all()
#         serializer = GuichetsSerializer(guichets, many=True)
#         return Response(serializer.data)

#     elif request.method == 'POST':
#         serializer = GuichetsSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# @api_view(['GET', 'PUT', 'DELETE'])
# def guichet_detail(request, pk):
#     try:
#         guichet = Guichets.objects.get(pk=pk)
#     except Guichets.DoesNotExist:
#         return Response(status=status.HTTP_404_NOT_FOUND)

#     if request.method == 'GET':
#         serializer = GuichetsSerializer(guichet)
#         return Response(serializer.data)

#     elif request.method == 'PUT':
#         serializer = GuichetsSerializer(guichet, data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#     elif request.method == 'DELETE':
#         Guichets.delete()
#         return Response(status=status.HTTP_204_NO_CONTENT)
