from statistics import mode
from django.db import models

TypeAgent = (
    ('naissance','naissance'),
    ('renouvellement','renouvellement'),
    ('mariage','mariage'),
    ('deces','deces'),
    ('authentication','authentication'),
    ('genre','genre'),
)     
class Agent(models.Model):
    prenom = models.CharField(max_length=255)
    nom = models.CharField(max_length=255)
    telephone = models.CharField(max_length=255)
    email = models.EmailField()
    login = models.CharField(max_length=255, unique=True)
    password = models.CharField(max_length=255)
    typeAgent = models.CharField(max_length=255, choices=TypeAgent)
    etat = models.BooleanField(default=True)
    

    
