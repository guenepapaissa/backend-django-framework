# Generated by Django 4.0.3 on 2022-03-27 02:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agent', '0004_agent_guichet'),
    ]

    operations = [
        migrations.AddField(
            model_name='agent',
            name='etat',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='guichets',
            name='guichet',
            field=models.CharField(choices=[('naissance', 'naissance'), ('renouvellement', 'renouvellement'), ('mariage', 'mariage'), ('deces', 'deces'), ('authentication', 'authentication'), ('genre', 'genre')], max_length=255),
        ),
    ]
