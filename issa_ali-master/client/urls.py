from django import views
from django.urls import path
from client.views import client_list,client_detail

urlpatterns = [
    path('client/', client_list ),
    path('client/<int:pk>', client_detail),
]
