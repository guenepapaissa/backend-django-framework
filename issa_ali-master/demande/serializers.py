from rest_framework.serializers import ModelSerializer
from .models import *

class DemandeSerializer(ModelSerializer):
    class Meta:
        model = Demande
        fields = '__all__'
        
class MotifsSerializer(ModelSerializer):
    class Meta:
        model = Motifs
        fields = '__all__'
        
