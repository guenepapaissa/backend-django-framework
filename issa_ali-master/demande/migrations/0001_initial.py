# Generated by Django 4.0.3 on 2022-03-26 02:56

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('agent', '0002_rename_typeagent_agent_typeagent'),
    ]

    operations = [
        migrations.CreateModel(
            name='Motifs',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('motif', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Demande',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('prenom', models.CharField(max_length=255)),
                ('nom', models.CharField(max_length=255)),
                ('telephone', models.CharField(max_length=255)),
                ('typeDemande', models.CharField(choices=[('naissance', 'naissance'), ('renouvellement', 'renouvellement'), ('mariage', 'mariage'), ('deces', 'deces'), ('residence', 'residence'), ('collective', 'collective'), ('individuel', 'individuel')], max_length=255)),
                ('agent', models.ManyToManyField(to='agent.agent')),
                ('motif', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='demande.motifs')),
            ],
        ),
    ]
