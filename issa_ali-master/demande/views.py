from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import *
from .serializers import *


@api_view(['GET', 'POST'])
def demande_list(request):
    
    
    if request.method == 'GET':
        demandes = Demande.objects.all()
        serializer = DemandeSerializer(demandes, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = DemandeSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'POST'])
def motif_rejet(request):
    if request.method == 'GET':
        motifs = Motifs.objects.all()
        serializer = MotifsSerializer(motifs, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = MotifsSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'PUT', 'DELETE'])
def motif_detail(request, pk):
    try:
        motif = Motifs.objects.get(pk=pk)
    except Motifs.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = MotifsSerializer(motif)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = MotifsSerializer(motif, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        Motifs.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)



@api_view(['GET', 'POST'])
def demande_list_naissance(request):  
    if request.method == 'GET':
        declarationNaissances = Demande.objects.filter(typeDemande="Dnaissance")
        demandes = Demande.objects.all()
        serializer = DemandeSerializer(declarationNaissances, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = DemandeSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'POST'])
def demande_list_mariage(request):  
    if request.method == 'GET':
        declarationMariages = Demande.objects.filter(typeDemande="Dmariage")
        demandes = Demande.objects.all()
        serializer = DemandeSerializer(declarationMariages, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = DemandeSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'POST'])
def demande_list_deces(request):  
    if request.method == 'GET':
        declarationDecess = Demande.objects.filter(typeDemande="Ddeces")
        demandes = Demande.objects.all()
        serializer = DemandeSerializer(declarationDecess, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = DemandeSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'POST'])
def demande_list_authentification(request):  
    if request.method == 'GET':
        authentifications = Demande.objects.filter(typeDemande="authentification")
        demandes = Demande.objects.all()
        serializer = DemandeSerializer(authentifications, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = DemandeSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'POST'])
def demande_list_renouvellement(request):  
    if request.method == 'GET':
        renouvellements = Demande.objects.filter(typeDemande="Rnaissance,Rdeces,Rmariage")
        demandes = Demande.objects.all()
        serializer = DemandeSerializer(renouvellements, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = DemandeSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'POST'])
def demande_list_toutGenre(request):  
    if request.method == 'GET':
        toutGenres = Demande.objects.filter(typeDemande="residence,collective,individuel")
        demandes = Demande.objects.all()
        serializer = DemandeSerializer(toutGenres, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = DemandeSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)