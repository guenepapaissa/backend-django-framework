from django import views
from django.urls import path
from demande.views import * 

urlpatterns = [
    path('demande/', demande_list ),
    path('motif/', motif_rejet ),
    path('motif/<int:pk>', motif_detail ),
   # path('demande/<int:pk>', demande_detail),
    # path('demande_naissance/', demande_list_naissance ),
    # path('demande_mariage/', demande_list_mariage ),
    # path('demande_deces/', demande_list_deces ),
    # path('demande_renouvellement/', demande_list_renouvellement ),
    # path('demande_toutGenre/', demande_list_toutGenre ),
    # path('demande_authentification/', demande_list_authentification ),
]
