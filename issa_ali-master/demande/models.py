from django.db import models

from agent.models import Agent

TypeDemande = (
    ('Dnaissance','Dnaissance'),
     ('Rnaissance','Rnaissance'),
    ('Dmariage','Dmariage'),
     ('Rmariage','Rmariage'),
    ('Ddeces','Ddeces'),
    ('Rdeces','Rdeces'),
    ('residence','residence'),
    ('collective','collective'),
    ('individuel','individuel'),
    ('authentification','authentification'),

) 
 
class Motifs(models.Model):
    motif = models.CharField(max_length=255)
    
    
class Demande(models.Model):
    prenom = models.CharField(max_length=255)
    nom = models.CharField(max_length=255)
    telephone = models.CharField(max_length=255)

    typeDemande = models.CharField(max_length=255, choices=TypeDemande)
    motif =models.ForeignKey(Motifs,null=True, on_delete=models.CASCADE)
    agent = models.ManyToManyField(Agent)
    


