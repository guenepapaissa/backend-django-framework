from django.apps import AppConfig


class AuthentifationConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'authentifation'
