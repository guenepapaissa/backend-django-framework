from http import client
from multiprocessing.connection import Client
from django.shortcuts import redirect, render
import pickle

from administrateur.serializers import AdministrateurSerializer
from agent.serializers import AgentSerializer
from officier.serializers import OfficierSerializer
from client.serializers import ClientSerializer
from agent.models import Agent
from administrateur.models import Administrateur
from officier.models import Officier
from client.models import Client
from rest_framework.views import APIView
from rest_framework.response import Response
from .serializers import *
from rest_framework.decorators import api_view
from rest_framework import status

@api_view(['POST'])
def login(request):
    if request.method == 'POST':
        login = request.POST.get('login').split('"')[1]
        password = request.POST.get('password').split('"')[1]
        
        administrateur = Administrateur.objects.filter(login=login,password=password)
        agent = Agent.objects.filter(login=login,password=password)
        officier = Officier.objects.filter(login=login,password=password)
        client = Client.objects.filter(login=login,password=password)
        print(len(administrateur))

        if len(administrateur) == 1 :
            utilisateur = Administrateur.objects.get(login=login,password=password)
            if utilisateur.etat == True:
                administrateurSerializer = AdministrateurSerializer(utilisateur,many=False)
                request.session['login'] = login
                uti = administrateurSerializer.data
                uti['profil'] ='administrateur'
                return Response(uti)
        elif len(agent) == 1 :
            utilisateur = Agent.objects.get(login=login,password=password)
            if utilisateur.etat == True:
                request.session['login'] = login
                agentSerializer = AgentSerializer(utilisateur,many=False)
                uti = agentSerializer.data
                uti['profil'] ='agent'
                return Response(uti)
        elif len(officier) == 1 : 
            utilisateur = Officier.objects.get(login=login,password=password)
            if utilisateur.etat == True: 
                request.session['login'] = login
                officierSerializer = OfficierSerializer(utilisateur,many=False)
                uti = officierSerializer.data
                uti['profil'] ='officier'
                return Response(uti)
        elif len(client) == 1 : 
            utilisateur = Client.objects.get(login=login,password=password)
            if utilisateur.etat == True: 
                request.session['login'] = login
                clientSerializer = ClientSerializer(utilisateur,many=False)
                uti = clientSerializer.data
                uti['profil'] ='client'
                return Response(uti)
            
        return Response("erreur")

# se déconnecté a l'application
@api_view(['POST'])
def disconnect(request):
    request.session.clear()
    return redirect('login')