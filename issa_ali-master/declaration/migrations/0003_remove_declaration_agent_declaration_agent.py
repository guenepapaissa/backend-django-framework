# Generated by Django 4.0.3 on 2022-04-05 16:40

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('agent', '0006_remove_agent_guichet_delete_guichets'),
        ('declaration', '0002_declaration_rendezvous'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='declaration',
            name='agent',
        ),
        migrations.AddField(
            model_name='declaration',
            name='agent',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='agent.agent'),
        ),
    ]
