# from http import client
from client.models import Client
from django.db import models
from demande.models import Motifs
from agent.models import Agent

Extrait = (
    ('Naissance','Naissance'),
     ('Mariage','Mariage'),
    ('Deces','Deces'),
)
 

class Declaration(models.Model):
    date = models.DateField(auto_now=True)
    etat = models.BooleanField(default=False)
    client = models.ForeignKey(Client,null=True, on_delete=models.CASCADE)
    motif =models.ForeignKey(Motifs,null=True, on_delete=models.CASCADE)
    agent =models.ForeignKey(Agent,null=True, on_delete=models.CASCADE)
    dateEvenement = models.DateField()
    rendezVous = models.DateField(null=True)
    extrait = models.CharField(max_length=255, choices=Extrait)
    
