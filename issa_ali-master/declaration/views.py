from http import client
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import *
from .serializers import *


@api_view(['GET', 'POST'])
def declaration_list(request):
    
    
    if request.method == 'GET':
        declarations = Declaration.objects.all()
        serializer = DeclarationSerializer(declarations, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = DeclarationSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    
@api_view(['GET', 'POST'])
def declaration_naissance_list(request):
    
    
    if request.method == 'GET':
        declarations = Declaration.objects.filter(extrait='Naissance')
        serializer = DeclarationSerializer(declarations, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = DeclarationSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    
@api_view(['GET', 'POST'])
def declaration_deces_list(request):
    
    
    if request.method == 'GET':
        declarations = Declaration.objects.filter(extrait='Deces')
        serializer = DeclarationSerializer(declarations, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = DeclarationSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    
@api_view(['GET', 'POST'])
def declaration_mariage_list(request):
    
    
    if request.method == 'GET':
        declarations = Declaration.objects.filter(extrait='Mariage')
        serializer = DeclarationSerializer(declarations, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = DeclarationSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    


@api_view(['GET', 'PUT', 'DELETE'])
def declaration_detail(request, pk):
    try:
        declaration = Declaration.objects.get(pk=pk)
    except Declaration.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = DeclarationSerializer(declaration)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = DeclarationSerializer(declaration, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        Declaration.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

@api_view(['GET'])
def eneleverMotif(request,pk):
    try:
        declaration = Declaration.objects.get(pk=pk)
        declaration.motif=None
        declaration.etat=False
        declaration.rendezVous=None
        declaration.save()
        serializer = DeclarationSerializer(declaration,many=False)
        return Response(serializer.data)
    except Declaration.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

@api_view(['GET', 'PUT', 'DELETE'])
def mes_declarations(request, pk):
    try:
        declaration = Declaration.objects.filter(client=pk)
    except Declaration.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = DeclarationSerializer(declaration,many=True)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = DeclarationSerializer(declaration, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        Declaration.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

