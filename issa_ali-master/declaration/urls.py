from django import views
from django.urls import path
from declaration.views import declaration_list,declaration_detail,declaration_naissance_list,declaration_deces_list,declaration_mariage_list, mes_declarations,eneleverMotif
urlpatterns = [
    path('declaration/', declaration_list ),
    path('declaration_naissance/', declaration_naissance_list ),
    path('declaration_deces/', declaration_deces_list ),
    path('declaration_mariage/', declaration_mariage_list ),
    path('mesdeclaration/<int:pk>', mes_declarations),
    path('declaration/<int:pk>', declaration_detail),
    path('eneleverMotif/<int:pk>', eneleverMotif),

]
