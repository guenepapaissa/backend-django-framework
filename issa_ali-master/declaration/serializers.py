from rest_framework.serializers import ModelSerializer
from .models import *

class DeclarationSerializer(ModelSerializer):
    class Meta:
        model = Declaration
        fields = '__all__'
        

